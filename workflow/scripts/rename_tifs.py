import os
from glob import glob
import sys

#numerical prefix that is not separated from the actual image index confuses the dxchange filename parser, 
# so it is better to first rename the files. This function renames the files into 00000.tif, 00001.tif, ...
def rename_tifs(path_to_directory):
    fnames = glob(path_to_directory + '/' + '*.tif')
    fnames.sort()
    for f in range(len(fnames)):
        new_name = path_to_directory + '/' + '{:0>5}'.format(str(f+1)) + '.tif'
        os.rename(fnames[f], new_name)
    with open(snakemake.log[0], "w") as f:
        sys.stderr = sys.stdout = f
        f.write('success')

def main():
    path_to_directory = snakemake.input[0]
    rename_tifs(path_to_directory)

if __name__ == '__main__':
    main()
