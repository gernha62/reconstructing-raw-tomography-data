import tomopy
import dxchange
import numpy as np
from glob import glob

def reconstruction(path_to_output, tifFile, numDarks, numFlats, numProjs, rot_center_init):
    #Load Raw Data into Memory
    tifRef = tifFile
    firstIndex = 1
    #Read Darks
    darkStart = firstIndex
    darkStop = darkStart + numDarks 
    dark = dxchange.reader.read_tiff_stack(tifRef, [i for i in range(darkStart, darkStop)])
    #Read Flats
    flatStart = darkStop
    flatStop = flatStart + numFlats
    flat = dxchange.reader.read_tiff_stack(tifRef, [i for i in range(flatStart, flatStop)])
    #Read Projections
    projStart = flatStop
    projStop = projStart + numProjs
    proj = dxchange.reader.read_tiff_stack(tifRef, [i for i in range(projStart, projStop)])
    print ("Shape of projection cube", proj.shape)
    #Define equiangular angles (angles here not part of raw data)
    theta = tomopy.angles(proj.shape[0])
    #Start Reconstruction
    #Correct for dark and flat
    threads = 12
    proj = tomopy.normalize(proj, flat, dark, ncore=threads)
    proj = tomopy.minus_log(proj, ncore=threads)
    rot_center = rot_center_init
    #Inverse Radon
    recon = tomopy.recon(proj, theta, center=rot_center, algorithm='gridrec', sinogram_order=False, ncore=threads)
    #Cosmetics
    recon = tomopy.circ_mask(recon, axis=0, ratio=0.95, ncore=threads)
    print("Reconstruction finished")
    dxchange.write_tiff_stack(recon, fname=path_to_output + '/recon')
    print ("Finished")

def main():
    path_to_directory = snakemake.input[0]
    fnames = glob(path_to_directory + '/' + '*.tif')
    tiff_file_example = fnames[0]
    path_to_output = snakemake.output[0]
    numDarks_magma = snakemake.params.numDarks
    numFlats_magma = snakemake.params.numFlats
    numProjs_magma = snakemake.params.numProjs
    rot_center_magma = snakemake.params.rotCenter
    reconstruction(path_to_output, tiff_file_example, numDarks_magma, numFlats_magma, numProjs_magma, rot_center_magma)

if __name__ == '__main__':
    main()
