# Snakemake workflow: Reconstructing raw tomography data

A Snakemake worfklow for tomographically reconstructing raw data using [tomopy](https://tomopy.readthedocs.io/en/stable/).

## Installation

First download this repo and navigate to it
```bash
git clone https://codebase.helmholtz.cloud/gernha62/reconstructing-raw-tomography-data.git
```
```bash
cd /path/to/repo
```
(Optional) Download the example folder with:
```bash
wget -m -np https://doi2.psi.ch/datasets/das/work/p15/p15869/compression/MI04_02/tif
```
Create a virtual environment and install all necessary packages (requires conda): 
```bash
conda env create --name reconstr_env --file workflow/envs/reconstr.yml
```
Activate the new virtual environment: 
```bash
conda activate reconstr_env
```

## Configuration

To configure the workflow, adapt the config file found at `config/config.yaml` . The config looks as follows:
```yaml
number_of_darks: 50
number_of_flats: 100
number_of_projections: 501
rotation_center: 508.77
raw_data:
  MI04_02: doi2.psi.ch/datasets/das/work/p15/p15869/compression/MI04_02/tif
```
 In the config, adjust `number_of_darks`, `number_of_flats`, `number_of_projections` and `rotation_center` to the number of darks, flats, projections and the rotation center of your dataset. The necessary information can usually be found in the .log file of the folder that contains the raw data. 

`MI04_02: doi2.psi.ch/datasets/das/work/p15/p15869/compression/MI04_02/tif` denotes the path to the example folder used for reconstruction and the keyword `MI04_02` will be used to name the output (e.g. in this case the output folder will be named `recon_dir_MI04_02`). Replace the examle path with the path to the dataset you want to reconstruct. Additionally, if you want the name of the output folder to have a different suffix, replace the keyword `MI04_02` with a name you prefer.

## Run the workflow

If the .tif files contain a numerical prefix that is not separated from the actual image index, it is best to first rename the files. The files will be renamed to `00001.tif`, `00002.tif` and so on. If the renaming is needed, run:

```bash
snakemake --cores 1 'logs/renamefile_MI04_02.log'
```
If you replaced the keyword `MI04_02` in the config file then adjust the command accordingly (e.g. if you replaced the keyword with `Tomo_dataset` then the command should be `snakemake --cores 1 'logs/renamefile_Tomo_dataset.log'`).

Before trying to compute the reconstructions, make sure you have enough memory available (ideally more than 60 GB).
To compute the reconstructions using one core, use the command:
```bash
snakemake --cores 1
```
If you want to use all available cores instead, use:
```bash
snakemake --cores all
```
This creates a folder in `results` with the reconstructed data.

## Credit
The example dataset used in this project (MI04_02 evolving magma, Mattia Pistone, University of Georgia) was taken from: https://doi.psi.ch/detail/10.16907/05a50450-767f-421d-9832-342b57c201af

The script used for reconstruction (`scripts/reconstructs_tomo_datasets.py`) was provided by Alain Studer, PSI.